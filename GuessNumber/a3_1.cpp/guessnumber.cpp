//
//  main.cpp
//  guessnumber.cpp
//
//  Created by ReenaSing on 9/2/15.
//  Copyright (c) 2015 ReenaSing. All rights reserved.
//

#include <iostream> // ANSI Standard
using namespace std;

// Function prototype
void playOneGame();
void getUserResponseToGuess(int guess, char& result);
int  getMidpoint(int low, int high);


//Global variable
int guess;
int midPt;


// C++ program begins at main
int main() {
    
    char response;
    cout << "Ready to play (y/n)? ";
    cin >> response;
    
    while (response == 'y') {
        
        //entry point of function
        playOneGame();
        cout << "Great! Do you want to play again (y/n)? ";
        cin >> response;
    }
}

// Method definition

void playOneGame() {
    
    // local varible
    int g;
    char result;
    
    // Insertion operator << use to display output to console
    cout << "Think of a number between 1 and 100" << "\n";
    
    // Accept user input
    cin >> g;
    guess = g;
    
    //entry point of function inside function
    getUserResponseToGuess(g, result);
    
}

void getUserResponseToGuess(int guess, char& result){

    char userInput;
    cout << "User Entered:" << guess << "\n";
   
    //declaring local variable
    int low = 0;
    int high = 100;
    int newMidPt;
    
    //entry point of function getMidpoint
    midPt = getMidpoint(low, high);
    cout << "Is it " << midPt << " (h/l/c)?" << "\n";
        
    while (low <= high) {
        
        bool leave = false;
        cin >> userInput;
        
        // for loop execution
        for (int i = 0; i< 10; i++){
        
            //check the boolean condition -- all if statements
            if(guess > midPt) {
        
                newMidPt = midPt;
                low = newMidPt + 1;
                newMidPt = getMidpoint(low, high);
                cout << "Is it " << newMidPt << " (h/l/c)?" << "\n";
                cin >> userInput;
                midPt = newMidPt;
                if(userInput == 'c' && guess == midPt){
                    
                    cout << "got it Correct";
                    leave = true;
                }
            }
            
            if(guess < midPt) {
                
                newMidPt = midPt;
                high = newMidPt - 1;
                low =  low + 1;
                newMidPt = getMidpoint(low, high);
                cout << "Is it" << newMidPt << "(h/l/c)?" << "\n";
                cin >> userInput;
                midPt = newMidPt;
                
                // some numbers are not possible to get with midpoint,
                // (1) & (2) logic helps to get particular number. e.g 77
                
                //(1)
                
                if(userInput == 'l' && guess + 1 == midPt){
                
                    midPt = midPt - 1;
                    cout << "Is it" << midPt << "(h/l/c)?" << "\n";
                    cin >> userInput;
                }
                
                //(2)
                
                if(userInput == 'l' && guess + 3 == midPt){
                    
                    midPt = midPt - 3;
                    cout << "Is it" << midPt << "(h/l/c)?" << "\n";
                    cin >> userInput;
                }
                //(3)
                
                if(userInput == 'c' && guess == midPt){
                   
                    cout << "got it Correct" << "\n";
                    leave = true;
                }
                
            }
                
            if (guess == midPt) {
                    
                newMidPt = midPt;
                cout << "\nIt is " << newMidPt <<  "\n";
                cout << "Great! Do you want to play again (y/n)?" ;
                cin >> userInput;
                
                if (userInput == 'y'){
                    playOneGame();
                }
                
                if (userInput == 'n'){
                
                    break; // terminate the loop
                }
                break; // terminate the loop
            }
            
        }
        
    }
    
}


int getMidpoint(int low, int high){
    
    int midPoint = 0;
    midPoint = (low + high) / 2;
    return midPoint;
}

// Result:

/***********************
 
 Ready to play (y/n)? y
 Think of a number between 1 and 100
 1
 User Entered:1
 Is it 50 (h/l/c)?
 l
 Is it25(h/l/c)?
 l
 Is it13(h/l/c)?
 l
 Is it7(h/l/c)?
 l
 Is it5(h/l/c)?
 l
 Is it4(h/l/c)?
 l
 Is it1(h/l/c)?
 c
 got it Correct
 
 It is 1
 Great! Do you want to play again (y/n)?y
 Think of a number between 1 and 100
 22
 User Entered:22
 Is it 50 (h/l/c)?
 l
 Is it25(h/l/c)?
 l
 Is it22(h/l/c)?
 c
 got it Correct
 
 It is 22
 Great! Do you want to play again (y/n)?y
 Think of a number between 1 and 100
 45
 User Entered:45
 Is it 50 (h/l/c)?
 l
 Is it25(h/l/c)?
 h
 Is it 37 (h/l/c)?
 h
 Is it 43 (h/l/c)?
 h
 Is it 46 (h/l/c)?
 l
 Is it45(h/l/c)?
 c
 got it Correct
 
 It is 45
 Great! Do you want to play again (y/n)?y
 Think of a number between 1 and 100
 55
 User Entered:55
 Is it 50 (h/l/c)?
 h
 Is it 75 (h/l/c)?
 l
 Is it63(h/l/c)?
 l
 Is it57(h/l/c)?
 l
 Is it55(h/l/c)?
 c
 got it Correct
 
 It is 55
 Great! Do you want to play again (y/n)?y
 Think of a number between 1 and 100
 77
 User Entered:77
 Is it 50 (h/l/c)?
 h
 Is it 75 (h/l/c)?
 h
 Is it 88 (h/l/c)?
 l
 Is it82(h/l/c)?
 l
 Is it79(h/l/c)?
 l
 Is it78(h/l/c)?
 l
 Is it77(h/l/c)?
 c
 got it Correct
 
 It is 77
 Great! Do you want to play again (y/n)?y
 Think of a number between 1 and 100
 98
 User Entered:98
 Is it 50 (h/l/c)?
 h
 Is it 75 (h/l/c)?
 h
 Is it 88 (h/l/c)?
 h
 Is it 94 (h/l/c)?
 h
 Is it 97 (h/l/c)?
 h
 Is it 99 (h/l/c)?
 l
 Is it98(h/l/c)?
 c
 got it Correct
 
 It is 98
 Great! Do you want to play again (y/n)?n
 
 
 
 ***********************/
