/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : creature.h
 Created by Reena Singh on 12/04/15
 Copyright (c) 2015 ReenaSing. All rights reserved. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#ifndef creature_H
#define creature_H

#include <iostream>

using namespace std;

namespace cs_creature {

    class creature {
        
    
        public:
        
            creature();
            creature(int myStrength, int myHitpoints);
        
            int getStrength()const;
            int getHitpoints()const;
        
            void setStrength(int newStrength);
            void setHitpoints(int newHitpoints);
        
            virtual int getDamage()const;
            virtual string getSpecies()const;
        
        private:
            int strength;
            int hitpoints;
        
    };
    
    
}
#endif


/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
// Test: 1
/*
 
 ----------------------------------
 battleArena between elf and balrog
 ----------------------------------
 Initial hitpoint for elf 50
 Initial hitpoint for balrog 50
 
 The elf attacks for 19 points!
 Magical attack inflicts 19 additional damage points!
 The balrog attacks for 46 points!
 Balrog speed attack inflicts 26 additional damage points!
 
 Damage made by elf 38
 Damage made by balrog 72
 
 elf after minus damage of balrog has remaining -22 hit points.
 balrog after minus damage of elf has remaining 12 hit points.
 
 balrog is winner
 
 
 -----------------------------------------
 battleArena between human and cyberdemon
 ----------------------------------------
 Initial hitpoint for cyberdemon 50
 Initial hitpoint for human 50
 
 The cyberdemon attacks for 49 points!
 The human attacks for 34 points!
 
 Damage made by cyberdemon 49
 Damage made by human 34
 
 cyberdemon after minus damage of human has remaining 16 hit points.
 human after minus damage of cyberdemon has remaining 1 hit points.
 
 The cyberdemon attacks for 9 points!
 Demonic attack inflicts 50 additional damage points!
 The human attacks for 20 points!
 
 Damage made by cyberdemon 59
 Damage made by human 20
 
 cyberdemon after minus damage of human has remaining -4 hit points.
 human after minus damage of cyberdemon has remaining -58 hit points.
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between cyberdemon and elf
 -------------------------------------
 Initial hitpoint for elf -22
 Initial hitpoint for cyberdemon -4
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between human and balrog
 ------------------------------------
 Initial hitpoint for human -58
 Initial hitpoint for balrog 12
 
 balrog is winner
 
 
 -----------------------------------------
 battleArena between human and elf
 ---------------------------------
 Initial hitpoint for human -58
 Initial hitpoint for elf -22
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between cyberdemon and balrog
 -----------------------------------------
 Initial hitpoint for cyberdemon -4
 Initial hitpoint for balrog 12
 
 balrog is winner
 
 
 -----------------------------------------
 
 */

// Test:2

/*
 ----------------------------------
 battleArena between elf and balrog
 ----------------------------------
 Initial hitpoint for elf 50
 Initial hitpoint for balrog 50
 
 The elf attacks for 28 points!
 Magical attack inflicts 28 additional damage points!
 The balrog attacks for 43 points!
 Balrog speed attack inflicts 6 additional damage points!
 
 Damage made by elf 56
 Damage made by balrog 49
 
 elf after minus damage of balrog has remaining 1 hit points.
 balrog after minus damage of elf has remaining -6 hit points.
 
 elf is winner
 
 
 -----------------------------------------
 battleArena between human and cyberdemon
 ----------------------------------------
 Initial hitpoint for cyberdemon 50
 Initial hitpoint for human 50
 
 The cyberdemon attacks for 30 points!
 Demonic attack inflicts 50 additional damage points!
 The human attacks for 16 points!
 
 Damage made by cyberdemon 80
 Damage made by human 16
 
 cyberdemon after minus damage of human has remaining 34 hit points.
 human after minus damage of cyberdemon has remaining -30 hit points.
 
 cyberdemon is winner
 
 
 -----------------------------------------
 battleArena between cyberdemon and elf
 -------------------------------------
 Initial hitpoint for elf 1
 Initial hitpoint for cyberdemon 34
 
 The elf attacks for 6 points!
 Magical attack inflicts 6 additional damage points!
 The cyberdemon attacks for 32 points!
 Demonic attack inflicts 50 additional damage points!
 
 Damage made by elf 12
 Damage made by cyberdemon 82
 
 elf after minus damage of cyberdemon has remaining -81 hit points.
 cyberdemon after minus damage of elf has remaining 22 hit points.
 
 cyberdemon is winner
 
 
 -----------------------------------------
 battleArena between human and balrog
 ------------------------------------
 Initial hitpoint for human -30
 Initial hitpoint for balrog -6
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between human and elf
 ---------------------------------
 Initial hitpoint for human -30
 Initial hitpoint for elf -81
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between cyberdemon and balrog
 -----------------------------------------
 Initial hitpoint for cyberdemon 22
 Initial hitpoint for balrog -6
 
 cyberdemon is winner
 
 
 -----------------------------------------
 */

// Test: 3

/*
 
 ----------------------------------
 battleArena between elf and balrog
 ----------------------------------
 Initial hitpoint for elf 50
 Initial hitpoint for balrog 50
 
 The elf attacks for 40 points!
 Magical attack inflicts 40 additional damage points!
 The balrog attacks for 22 points!
 Balrog speed attack inflicts 22 additional damage points!
 
 Damage made by elf 80
 Damage made by balrog 44
 
 elf after minus damage of balrog has remaining 6 hit points.
 balrog after minus damage of elf has remaining -30 hit points.
 
 elf is winner
 
 
 -----------------------------------------
 battleArena between human and cyberdemon
 ----------------------------------------
 Initial hitpoint for cyberdemon 50
 Initial hitpoint for human 50
 
 The cyberdemon attacks for 10 points!
 The human attacks for 1 points!
 
 Damage made by cyberdemon 10
 Damage made by human 1
 
 cyberdemon after minus damage of human has remaining 49 hit points.
 human after minus damage of cyberdemon has remaining 40 hit points.
 
 The cyberdemon attacks for 11 points!
 The human attacks for 7 points!
 
 Damage made by cyberdemon 11
 Damage made by human 7
 
 cyberdemon after minus damage of human has remaining 42 hit points.
 human after minus damage of cyberdemon has remaining 29 hit points.
 
 The cyberdemon attacks for 22 points!
 The human attacks for 5 points!
 
 Damage made by cyberdemon 22
 Damage made by human 5
 
 cyberdemon after minus damage of human has remaining 37 hit points.
 human after minus damage of cyberdemon has remaining 7 hit points.
 
 The cyberdemon attacks for 18 points!
 Demonic attack inflicts 50 additional damage points!
 The human attacks for 11 points!
 
 Damage made by cyberdemon 68
 Damage made by human 11
 
 cyberdemon after minus damage of human has remaining 26 hit points.
 human after minus damage of cyberdemon has remaining -61 hit points.
 
 cyberdemon is winner
 
 
 -----------------------------------------
 battleArena between cyberdemon and elf
 -------------------------------------
 Initial hitpoint for elf 6
 Initial hitpoint for cyberdemon 26
 
 The elf attacks for 40 points!
 Magical attack inflicts 40 additional damage points!
 The cyberdemon attacks for 49 points!
 
 Damage made by elf 80
 Damage made by cyberdemon 49
 
 elf after minus damage of cyberdemon has remaining -43 hit points.
 cyberdemon after minus damage of elf has remaining -54 hit points.
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between human and balrog
 ------------------------------------
 Initial hitpoint for human -61
 Initial hitpoint for balrog -30
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between human and elf
 ---------------------------------
 Initial hitpoint for human -61
 Initial hitpoint for elf -43
 
 The match is a tie for creatures!
 
 -----------------------------------------
 battleArena between cyberdemon and balrog
 -----------------------------------------
 Initial hitpoint for cyberdemon -54
 Initial hitpoint for balrog -30
 
 The match is a tie for creatures!
 
 -----------------------------------------
 
 */

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/