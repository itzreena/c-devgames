/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Program filename : demon.cpp
 Created by Reena Singh on 12/04/15.
 Copyright (c) 2015 ReenaSing. All rights reserved. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#include <iostream>
#include "demon.h"

using namespace std;

namespace cs_creature {

    
    demon::demon()
    {
        
    }
    
    
    
    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

    demon::demon(int myStrength, int myHitpoints)
    
    : creature(myStrength, myHitpoints)
    
    {
        
    }

    
    
    
    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
    

    int demon::getDamage() const{
        
        int damage = creature::getDamage();
        //cout << " attacks for " << damage << " points!" << endl;
        

        if (rand() % 4 == 0) {
            damage = damage + 50;
            cout << "Demonic attack inflicts 50 additional damage points!" << endl;
        }
        return damage;
    }
    
    

    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

    string demon::getSpecies() const{
        
        return "demon";
    }

    
    
    
    
    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
}