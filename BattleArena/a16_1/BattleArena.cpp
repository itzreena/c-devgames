/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Client filename : BattleArena.cpp
 Created by Reena Singh on 12/04/15.
 Copyright (c) 2015 ReenaSing. All rights reserved.
 
 */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#include <iostream>
#include <ctime>
#include <cstdlib>
#include "creature.h"
#include "human.h"
#include "balrog.h"
#include "demon.h"
#include "cyberdemon.h"
#include "elf.h"

using namespace std;
using namespace cs_creature;
void battleArena(creature &creature1, creature &creature2);

int main() {
    
    srand(time(0));
    
    human h(50, 50);
    cyberdemon c(50, 50);
    elf e(50, 50);
    balrog b(50, 50);
    
    cout << "----------------------------------" << endl;
    cout << "battleArena between elf and balrog" << endl << "----------------------------------" << endl;
    battleArena(e, b);
    cout << "battleArena between human and cyberdemon" << endl << "----------------------------------------" << endl;
    battleArena(c, h);
    cout << "battleArena between cyberdemon and elf" << endl << "-------------------------------------" << endl;
    battleArena(e, c);
    cout << "battleArena between human and balrog" << endl << "------------------------------------" << endl;
    battleArena(h, b);
    cout << "battleArena between human and elf" << endl << "---------------------------------" << endl;
    battleArena(h, e);
    cout << "battleArena between cyberdemon and balrog" << endl << "-----------------------------------------" << endl;
    battleArena(c, b);
    
    
}





void battleArena(creature &creature1, creature &creature2)
{
    int hitpt_c1, hitpt_c2, damage_c1, damage_c2;
    
    hitpt_c1 = creature1.getHitpoints();
    hitpt_c2 = creature2.getHitpoints();
    
    cout << "Initial hitpoint for " << creature1.getSpecies() << " " << hitpt_c1 << endl;
    cout << "Initial hitpoint for " << creature2.getSpecies() << " " << hitpt_c2 << endl;
    cout << endl;
    
    while ((hitpt_c1 > 0) && (hitpt_c2 > 0)) {
        
        damage_c1 = creature1.getDamage();
        damage_c2 = creature2.getDamage();
        
        cout << endl;
        cout << "Damage made by "<< creature1.getSpecies() << " " << damage_c1 << endl;
        cout << "Damage made by "<< creature2.getSpecies() << " " << damage_c2 << endl;
        cout << endl;
        
        hitpt_c1 = hitpt_c1 - damage_c2;
        cout << creature1.getSpecies() << " after minus damage of " << creature2.getSpecies()<< " has remaining " << hitpt_c1 << " hit points." << endl;
        
        hitpt_c2 = hitpt_c2 - damage_c1;
        cout << creature2.getSpecies() << " after minus damage of " << creature1.getSpecies()<< " has remaining " << hitpt_c2 << " hit points." << endl;
        
        creature1.setHitpoints(hitpt_c1);
        creature2.setHitpoints(hitpt_c2);
        cout << endl;
    }
    
    if ((hitpt_c1 <= 0) && (hitpt_c2 <= 0)) {
        
        cout << "The match is a tie for creatures!" << endl;
        
    }
    
    if ((hitpt_c1 <= 0) && (hitpt_c2 > 0))  {
        
        cout << creature2.getSpecies() << " is winner " << endl << endl;
        
    }
    
    if ((hitpt_c1 > 0) && (hitpt_c2 <= 0))  {
        
        cout << creature1.getSpecies() << " is winner " << endl << endl;
        
    }
    cout << endl;
    cout << "-----------------------------------------" << endl;
    
}
