
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Program filename : elf.cpp
 Created by Reena Singh on 12/04/15.
 Copyright (c) 2015 ReenaSing. All rights reserved. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
#include <iostream>
#include "elf.h"

using namespace std;

namespace cs_creature {

    
    elf::elf()
    {
        
    }

    
    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
    

    elf::elf(int myStrength, int myHitpoints)
    
    : creature(myStrength, myHitpoints)
    
    {
        
    }

    
    
    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
    
    
    int elf::getDamage() const{
        
        int damage = creature::getDamage();
        //cout << "The elf attacks for " << damage << " points!" << endl;

        if ((rand() % 2) == 0) {
            cout << "Magical attack inflicts " << damage << " additional damage points!" << endl;
            damage *= 2;
        }
        return damage;
    }


    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
    
    string elf::getSpecies() const{
        
        return "elf";
    }
    
    
    
    
    
    
    /*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

}