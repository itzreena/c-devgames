/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : elf.h
 Created by Reena Singh on 12/04/15
 Copyright (c) 2015 ReenaSing. All rights reserved. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/


#ifndef elf_H
#define elf_H

#include <iostream>
#include "creature.h"

using namespace std;

namespace cs_creature {

    class elf : public creature {
        
        public:
        
            elf();
            elf(int myStrength, int myHitpoints);
        
            int getDamage()const;
            string getSpecies()const;
  
    };

}

#endif