
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : balrog.h
 Created by Reena Singh on 12/04/15
 Copyright (c) 2015 ReenaSing. All rights reserved. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#ifndef balrog_H
#define balrog_H

#include <iostream>
#include "demon.h"

using namespace std;

namespace cs_creature {

    class balrog : public demon {
        
        public:
        
            balrog();
            balrog(int myStrength, int myHitpoints);
        
            int getDamage()const;
            string getSpecies()const;
        
    };
}

#endif