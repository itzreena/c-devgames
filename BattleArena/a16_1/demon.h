/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : demon.h
 Created by Reena Singh on 12/04/15
 Copyright (c) 2015 ReenaSing. All rights reserved. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#ifndef demon_H
#define demon_H

#include <iostream>
#include "creature.h"

using namespace std;

namespace cs_creature {

    class demon : public creature {
        
    public:
        
        demon();
        demon(int myStrength, int myHitpoints);
        
        int getDamage()const;
        string getSpecies()const;
    };

}

#endif