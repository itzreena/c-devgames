
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : cyberdemon.h
 Created by Reena Singh on 12/04/15
 Copyright (c) 2015 ReenaSing. All rights reserved. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
#ifndef cyberdemon_H
#define cyberdemon_H

#include <iostream>
#include "demon.h"

using namespace std;

namespace cs_creature {

class cyberdemon : public demon {
    
    public:
    
        cyberdemon();
        cyberdemon(int myStrength, int myHitpoints);
    
      //  int getDamage()const;
        string getSpecies()const;
    
    };

}

#endif