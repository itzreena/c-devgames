/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : human.h
 Created by Reena Singh on 12/04/15
 Copyright (c) 2015 ReenaSing. All rights reserved. 
 
 */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
#ifndef human_H
#define human_H

#include <iostream>
#include "creature.h"

using namespace std;

namespace cs_creature {
    
    class human : public creature {
    
        public:
        
            human();
            human(int myStrength, int myHitpoints);
        
          //  int getDamage()const;
            string getSpecies()const;
    };

}

#endif

