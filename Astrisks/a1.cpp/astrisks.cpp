//
//  main.cpp
//  astrisks.cpp
//
//  Created by ReenaSing on 8/31/15.
//  Copyright (c) 2015 ReenaSing. All rights reserved.
//

#include <iostream>
using namespace std;

int main() {
    
    //local variable declaration
    int numOfAstrisks;
    char response;
    
    //Prints output
    cout << "Would you like to print asterisks. Enter in Capital Letter (Y/N)?" << "\n";
    
    //Input by User
    cin >> response;
    
    //execute loop on true condition
    while (response != 'N') {
        
        
        cout << "How many asterisks?" << "\n";
        cin >> numOfAstrisks;
        
        //loop until the condition is met. i < numOfAstricks
        for(int i = 0; i < numOfAstrisks; i++) {
            cout << "*";
        }
        cout << endl;
        
        cout << "Would you like to print asterisks (Y/N)? " << "\n";
        cin >> response;
    }
    
}


//Output
/*
 
 Would you like to print asterisks (Y/N)?
 Y
 How many asterisks?
 6
 ******
 Would you like to print asterisks (Y/N)?
 Y
 How many asterisks?
 9
 *********
 Would you like to print asterisks (Y/N)?
 Y
 How many asterisks?
 0
 
 Would you like to print asterisks (Y/N)?
 Y
 How many asterisks?
 25
 *************************
 Would you like to print asterisks (Y/N)?
 N
 

*/