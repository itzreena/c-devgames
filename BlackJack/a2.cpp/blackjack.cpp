//
//  main.cpp
//  blackjack.cpp
//
//  Created by ReenaSing on 9/2/15.
//  Copyright (c) 2015 ReenaSing. All rights reserved.
//

#include <iostream>
using namespace std;


int main() {

    //Variable definition or local variable declaration
    int card, total, num1, num2;
    char response, nextPlay;
    
    //Prints output
    cout << "Would you like to play blackjack game. Enter in Capital Letter(Y/N)?" << "\n";
    
    //Input from player
    cin >> response;
    
    if(response != 'Y') {
        cout << "Game Ends";
    }
    
    // Execute loop while condition is true
    while (response != 'N'){
        
        /* initialize random seed: */
        srand((unsigned)time(NULL));
        
        cout << "First Cards: " << " ";
        
        //Actual Initialization of num1
        num1 = rand() % 11;
        
        //Prints output
        cout << num1 << "\n";
        cout << "First Cards: " << " ";
        
        //Actual Initialization of num2
        num2 = rand() % 11;
        cout << num1 << " "<< num2 << "\n";
        
        total = num1 + num2;
        cout << "Total: ";
        cout << total << "\n";
        
        cout << "Do you want another card (Y/N)? " << "\n";
        cin >> nextPlay;
        cout << "You Entered: " << nextPlay << "\n";
        
        //Execute loop while condition is true
        while(nextPlay != 'N'){
            
            card = rand() % 10 + 1;
            cout << "Card: ";
            cout << card << "\n";
            total = total + card;
            cout << "Total: ";
            cout << total << "\n";
            
            // if true, then prints output and break
            if (total >= 22){
                
                cout << "Bust" << "\n";
                break;
            }
            
            cout << "Do you want another card (Y/N)? ";
            cin >> nextPlay;
        
        } //Goes back to checking condition
        
        cout << "Would you like to play blackjack game(Y/N)?";
        cin >> response;
    }// Goes back to checking condition
}

/* Output of Program */

/********************************************
 
 Would you like to play blackjack game(Y/N)?
 Y
 First Cards:  4
 First Cards:  4 4
 Total: 8
 Do you want another card (Y/N)?
 Y
 You Entered: Y
 Card: 6
 Total: 14
 Do you want another card (Y/N)? Y
 Card: 3
 Total: 17
 Do you want another card (Y/N)? Y
 Card: 7
 Total: 24
 Bust
 Would you like to play blackjack game(Y/N)?Y
 First Cards:  3
 First Cards:  3 1
 Total: 4
 Do you want another card (Y/N)?
 Y
 You Entered: Y
 Card: 6
 Total: 10
 Do you want another card (Y/N)? Y
 Card: 7
 Total: 17
 Do you want another card (Y/N)? Y
 Card: 6
 Total: 23
 Bust
 Would you like to play blackjack game(Y/N)?Y
 First Cards:  7
 First Cards:  7 5
 Total: 12
 Do you want another card (Y/N)?
 Y
 You Entered: Y
 Card: 9
 Total: 21
 Do you want another card (Y/N)? Y
 Card: 1
 Total: 22
 Bust
 Would you like to play blackjack game(Y/N)?Y
 First Cards:  5
 First Cards:  5 5
 Total: 10
 Do you want another card (Y/N)?
 Y
 You Entered: Y
 Card: 8
 Total: 18
 Do you want another card (Y/N)? Y
 Card: 3
 Total: 21
 Do you want another card (Y/N)? Y
 Card: 2
 Total: 23
 Bust
 Would you like to play blackjack game(Y/N)?Y
 First Cards:  1
 First Cards:  1 7
 Total: 8
 Do you want another card (Y/N)?
 N
 You Entered: N
 Would you like to play blackjack game(Y/N)?N
 
 
 
 ********************************************/
