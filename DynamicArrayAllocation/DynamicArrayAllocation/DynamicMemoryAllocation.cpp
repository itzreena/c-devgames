/**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Program filename : DynamicMemoryAllocation.cpp
 Created by Reena Singh on 09/23/15.
 ReenaSing Copyright © 2015, All Right Reserved

 Program Description:
 Rewrite your High Scores program so that it uses Dynamic Memory Allocation to create the names and scores arrays.
 Making slight modifications to main function, the modified high scores program starts out by asking the user how many scores
 will be entered. It then allocate appropriate arrays, and then proceed just like the original High Scores assignment. */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#include <iostream>
using namespace std;

//function prototype
void initializeArrays(string names[], int scores[], int size);
void sortData(string names[], int scores[], int size);
void displayData(const string names[], const int scores[], int size);


int main(){
    
    //new pointer declared
    int *pvalue = NULL;
    
    //memory dynamic allocation
    pvalue = new int;
    
    //size of array is dynamic and is entered by user at console
    cout << "How many scores will be entered? ";
    cin >> *pvalue;
    const int size = *pvalue;
    
    //declaration of array's names and scores & dynamic memory allocation using new keyword
    
    int *scores = 0;
    scores = new int[size];
    string *names = new string[*pvalue];
   
    //Entry point of function's-> invoke three function here
    initializeArrays(names, scores, size);
    sortData(names, scores, size);
    displayData(names, scores, size);
  
    //memory freed or deallocated using delete keyword
    delete [] scores;
    
    //pointer changed to '0'
    scores = 0;
}//end main

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
// Function Definition

/* Function initializeArrays deal with all the user input. The data is stored in two arrays: an array of strings named names,
 and an array of ints named scores .*/

void initializeArrays(string names[], int scores[], int size){
    
    int count = 0;
    
    // Enter names and scores
    for (int i = 0; i < size ; i++){
        
        count = count + 1;
        cout << "Enter the name for score #"<< count << ": ";
        cin >> names[i];
        cout << "Enter the score for score #"<< count << ": ";
        cin >> scores[i];
        
    }//end for
    
}//end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

/* Function sortData()-> Takes three parameter names[], scores[] and size. The data stored in these two array's is sorted.
 Two value in array is compared in ascending order and exchange it places with each other if second value is higher than
 first value. This way swap is achieved between two scores. Whenever exchange takes place between two scores, then
 names is also exchanged in its place. */

void sortData(string names[], int scores[], int size){
    
    // sort by scores
    for (int i = size; i >= 0; i--) {
        
        for (int i = 0; i < size; i++) {
            
            
            if(scores[i] < scores[i+1]){
                
                //scores stored in memory is swaped
                int tempScores = scores[i+1];
                scores[i+1] = scores[i];
                scores[i] = tempScores;
                
                //names stored in memory is swaped
                string tempNames = names[i+1];
                names[i+1] = names[i];
                names[i] = tempNames;
                
            }//end if
        }//end for
    }//end for
    
}//end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

/*Function displayData -> takes three parameter names[], scores[] and Display's the final list of names and scores */

void displayData(const string names[], const int scores[], int size){
    
    cout << endl;
    cout << "Top Scorers: " << endl;
    
    
    for (int i = 0; i < size; i++){
        
        cout << names[i] << ": " << scores[i] << endl;
    
    }//end for
    
}//end function

//***********************Sample run of program***********************

/*
 
 How many scores will be entered? 6
 Enter the name for score #1: Reena
 Enter the score for score #1: 200
 Enter the name for score #2: Rose
 Enter the score for score #2: 400
 Enter the name for score #3: Jim
 Enter the score for score #3: 600
 Enter the name for score #4: Nora
 Enter the score for score #4: 300
 Enter the name for score #5: Natalie
 Enter the score for score #5: 700
 Enter the name for score #6: William
 Enter the score for score #6: 800
 
 Top Scorers:
 William: 800
 Natalie: 700
 Jim: 600
 Rose: 400
 Nora: 300
 Reena: 200
 
 */

