/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : myString.h
 Created by Reena Singh on 11/22/15.
 ReenaSing Copyright © 2015, All Right Reserved
 
 Description: The program specification myString class is written, to work with sequence of charaters a little more convenient 
 and less error-prone than handling raw c-strings. 
 For this require total 20 member function, one data member and a c-string implemented as a dynamic array.
 
 List of operation class supports:
 -A length member function which returns the number of characters in the string.
 -Printing a myString to a stream using an overloaded << (insertion) operator
 -myString object overloaded the square brackets [ ] operator to allow direct access
 -All six of the overloaded relational operators (<, <=, >, >=, ==, !=)
 -for 'a pointer as a data member'.  We have to use following member functions required in classes that use dynamic memory
 (1) Assignment operator (2) Copy constructor (3) Destructor (4) Default constructor
 - read() function reads file
 - Concatenation operator
 - Combined concatenation and assignment operator
 - Extraction operator.
 
 The Twenty member function is written for this program: Pre and Post Condition.
 
 [1]myString();
    //Preconditon: Takes no parameter and return nothing..not even void
    //Postcondition: initialize data member-'desc' with memory of size 1 and copy empty string to it
 
 [2]myString(const char *inDesc);
    //Preconditon: Takes one argument - pointer to char object 'inDesc' and is kept const
    //Postcondition: object is created  with size accordingly with specified object pointer given at input

 
 [3]myString(const myString& right);
    //Preconditon: Takes one argument - reference to myString object 'right' and is kept const
    //Postcondition: object is created  with size accordingly with specified object reference given at input

 
 [4]~myString();
    //Preconditon: an myString object exist with valid length
    //Postcondition: myString object is destroyed
 
 [5]myString operator=(const myString& right);
    //Preconditon: Input argument contains address to myString object:-Right.
    //Postcondition: object is copied  with size accordingly with specified object reference given at input
 
 [6]friend bool operator==(const myString& left, const myString& right);
    //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
    //Postcondition: function return's true, if Left == Right,  else it return's false.
 
 [7]friend bool operator<(const myString& left, const myString& right);
    //Precondition:- Input argument contains address to two myString object:- Left and Right.
    //Postcondition: function return's true, if Left < Right,  else it return's false.

 
 [8]friend bool operator!=(const myString& left, const myString& right);
    //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
    //Postcondition: function return's true, if Left != Right,  else it return's false.
 
 [9]friend bool operator>(const myString& left, const myString& right);
    //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
    //Postcondition: function return's true, if Left > Right,  else it return's false.
 
 [10]friend bool operator<=(const myString& left, const myString& right);
    //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
    //Postcondition: function return's true, if Left <= Right,  else it return's false.
 
 [11]friend bool operator>=(const myString& left, const myString& right);
    //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
    //Postcondition: function return's true, if Left >= Right,  else it return's false.
 
 [12]int length() const;
    //Preconditon: length donot take any argument
    //Postcondition: returns length of myString object
 
 [13]friend ostream& operator<<(ostream& out, const myString& source);
    //Precondition: The out of ostream object is waiting to receive the myString output.
    //Postcondition: myString source has been passed by reference to the ostream out and is kept const.
 
 [14]myString operator+=(const myString& right);
    //Preconditon: Input argument contains address to myString object:-Right.
    //Postcondition: returns pointer to this object which was created by combination of concatenation and assignment operator
 
 [15]myString operator+=(const char *right);
    //Preconditon: Input argument contains pointer to char object:-Right.
    //Postcondition: returns pointer to this char object which was created by combination of concatenation and assignment operator
 
 [16]friend const myString operator+(const myString &left, const myString &right);
    //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
    //Postcondition: function return's addition of two myString objects.
 
 [17]void read(istream &source, char delimit);
    //Preconditon: Input argument contains two argument, a stream and the delimiting character.
    //Postcondition: Reads character by character in a loop. Use the in.getline() function to do the reading of the input into a non-dynamic array, then use strcpy() to copy it into your data member. return void.
 
 [18]friend istream& operator>>(istream& in, myString &source);
    //Precondition: The in of istream object is waiting to receive the myString input.
    //Postcondition: myString source has been passed by reference into the istream 'in'.
 
 [19]char& operator[](int index);
   //Preconditon: Input argument contains value of index.
   //Postcondition: Returns the index to myString specifying the char to that location.
 
 [20]char operator[](int index) const;
   //Preconditon: Input argument contains value of index.
   //Postcondition: Returns the index to myString specifying the char to that location which is const.
 
 
 
*/
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#ifndef myString_H
#define myString_H

#include <iostream>

using namespace std;

namespace cs_myString {
    
    class myString {
        
    public:
    
        //DEFAULT CONSTRUCTOR
        
        myString();
        //Preconditon: Takes no parameter and return nothing..not even void
        //Postcondition: initialize data member-'desc' with memory of size 1 and copy empty string to it
        
        
        
        //CONSTRUCTOR
        
        myString(const char *inDesc);
        //Preconditon: Takes one argument - pointer to char object 'inDesc' and is kept const
        //Postcondition: object is created  with size accordingly with specified object pointer given at input
        
        
        
        //COPY CONSTRUCTOR
        
        myString(const myString& right);
        //Preconditon: Takes one argument - reference to myString object 'right' and is kept const
        //Postcondition: object is created  with size accordingly with specified object reference given at input
        
        
        
        //DESTRUCTOR
        
        ~myString();
        //Preconditon: an myString object exist with valid length
        //Postcondition: myString object is destroyed
        
        
        
        //ASSIGNMENT OPERATOR
        
        myString operator=(const myString& right);
        //Preconditon: Input argument contains address to myString object:-Right.
        //Postcondition: object is copied  with size accordingly with specified object reference given at input
        
        
        
        // CONCAT/ASSIGNMENT OPERATOR
        
        myString operator+=(const myString& right);
        //Preconditon: Input argument contains address to myString object:-Right.
        //Postcondition: returns pointer to this object which was created by combination of concatenation and assignment operator
        
        
        
        myString operator+=(const char *right);
        //Preconditon: Input argument contains pointer to char object:-Right.
        //Postcondition: returns pointer to this char object which was created by combination of concatenation and assignment operator
        
        int length() const;
        //Preconditon: length donot take any argument
        //Postcondition: returns length of myString object
        
        // OVERLOADING THE EXTRACTION OPERATOR
        
        friend ostream& operator<<(ostream& out, const myString& source);
        //Precondition: The out of ostream object is waiting to receive the myString output.
        //Postcondition: myString source has been passed by reference to the ostream out and is kept const.
        
        //OVERLOADED INSERTION OPERATOR
        
        friend istream& operator>>(istream& in, myString &source);
        //Precondition: The in of istream object is waiting to receive the myString input.
        //Postcondition: myString source has been passed by reference into the istream 'in'.
        
        //OVERLOADED All SIX OF THE RELATIONAL OPERATOR (<, <=, >, >=, ==, !=)
        
        friend bool operator<(const myString &Left, const myString &Right);
        //Precondition:- Input argument contains address to two myString object:- Left and Right.
        //Postcondition: function return's true, if Left < Right,  else it return's false.
        
        friend bool operator<=(const myString &Left, const myString &Right);
        //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
        //Postcondition: function return's true, if Left <= Right,  else it return's false.
        
        friend bool operator==(const myString &Left, const myString &Right);
        //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
        //Postcondition: function return's true, if Left == Right,  else it return's false.
        
        friend bool operator!=(const myString &Left, const myString &Right);
        //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
        //Postcondition: function return's true, if Left != Right,  else it return's false.
        
        friend bool operator>=(const myString &Left, const myString &Right);
        //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
        //Postcondition: function return's true, if Left >= Right,  else it return's false.
        
        friend bool operator>(const myString &Left, const myString &Right);
        //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
        //Postcondition: function return's true, if Left > Right,  else it return's false.
        
        //READ FROM FILE
        
        void read(istream& inString,char delim);
        //Preconditon: Input argument contains two argument, a stream and the delimiting character.
        //Postcondition: Reads character by character in a loop. Use the in.getline() function to do the reading of the input into a non-dynamic array, then use strcpy() to copy it into your data member. return void.
        
        //CONCAT OPERATOR
        
        friend const myString operator+(const myString &left, const myString &right);
        //Precondition:- Input parameters contains reference to two myString object:- Left and Right.
        //Postcondition: function return's addition of two myString objects.
       
        
        // OVERLOADING THE SQUARE BRACKETS
        
        char operator[](int index) const;
        //Preconditon: Input argument contains value of index.
        //Postcondition: Returns the index to myString specifying the char to that location which is const.
        
        
        char& operator[](int index);
        //Preconditon: Input argument contains value of index.
        //Postcondition: Returns the index to myString specifying the char to that location.
        
        
        
    private:
        
        char *desc; //POINTER DATA MEMBER
       
    };

    
}
#endif

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*(Test Run)**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 
 ----- Testing basic String creation & printing
 string [0] = Wow
 string [1] = C++ is neat!
 string [2] =
 string [3] = a-z
 
 ----- Now reading myStrings from file
 
 ----- first, word by word
 Read string = The
 Read string = first
 Read string = time
 Read string = we
 Read string = will
 Read string = read
 Read string = individual
 Read string = words,
 Read string = next
 Read string = we
 Read string = read
 Read string = whole
 Read string = lines
 
 ----- now, line by line
 Read string = The  first  time  we  will
 Read string =     read individual words, next
 Read string = we read whole lines
 
 ----- Testing access to characters (using const)
 Whole string is abcdefghijklmnopqsrtuvwxyz
 now char by char: abcdefghijklmnopqsrtuvwxyz
 ----- Testing access to characters (using non-const)
 Start with abcdefghijklmnopqsrtuvwxyz and convert to ABCDEFGHIJKLMNOPQSRTUVWXYZ
 
 ----- Testing relational operators between myStrings
 Comparing app to apple
	Is left < right? true
	Is left <= right? true
	Is left > right? false
	Is left >= right? false
	Does left == right? false
	Does left != right ? true
 Comparing apple to
	Is left < right? false
	Is left <= right? false
	Is left > right? true
	Is left >= right? true
	Does left == right? false
	Does left != right ? true
 Comparing  to Banana
	Is left < right? true
	Is left <= right? true
	Is left > right? false
	Is left >= right? false
	Does left == right? false
	Does left != right ? true
 Comparing Banana to Banana
	Is left < right? false
	Is left <= right? true
	Is left > right? false
	Is left >= right? true
	Does left == right? true
	Does left != right ? false
 
 ----- Testing relations between myStrings and char *
 Comparing he to hello
	Is left < right? true
	Is left <= right? true
	Is left > right? false
	Is left >= right? false
	Does left == right? false
	Does left != right ? true
 Comparing why to wackity
	Is left < right? false
	Is left <= right? false
	Is left > right? true
	Is left >= right? true
	Does left == right? false
	Does left != right ? true
 
 ----- Testing concatentation on myStrings
 outrageous + milk = outrageousmilk
 milk +  = milk
 + cow = cow
 cow + bell = cowbell
 
 ----- Testing concatentation between myString and char *
 abcde + XYZ = abcdeXYZ
 XYZ + abcde = XYZabcde
 
 ----- Testing shorthand concat/assign on myStrings
 who += what = whowhatandwhowhat
 what += WHEN = whatWHENandwhatWHEN
 WHEN += Where = WHENWhereandWHENWhere
 Where += why = WherewhyandWherewhy
 
 ----- Testing shorthand concat/assign using char *
 I love  += programming = I love programming
 
 ----- Testing copy constructor and operator= on myStrings
 original is cake, copy is fake
 original is cake, copy is fake
 after self assignment, copy is Copy Cat
 Testing pass & return myStrings by value and ref
 after calling Append, sum is BinkyBoo
 val is winky
 after assign,  val is BinkyBoo
 
 */