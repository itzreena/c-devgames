/*
  Program filename : playpoker.cpp
  Created by Reena Singh on 09/13/15.
 
 Program Description: The program is created to play Poker. The user enter 5 cards at console, then the 5 number is placed in 
 an array called hand. The program analyzes poker hand and prints whichever category it belongs.
 
 following are the category and its function that interpret results as follows.
 1) High : [ 2, 3, 4, 5, 9]
 2) Pair : [ 2 ,2, 3, 4, 6]
 3) Two Pair : [ 2, 2, 4, 4, 7]
 4) Three of a Kind : [5, 5, 3, 5, 7]
 5) Four of a Kind : [2, 5, 5, 5, 5]
 6) Full House : [3, 3, 3, 2, 2]
 7) Straight. [2, 3, 4, 5, 6]
 */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#include <iostream>
using namespace std;

//Function Prototype
bool  containsPair(const int hand[]);
bool  containsTwoPair(const int hand[]);
bool  containsThreeOfaKind(const int hand[]);
bool  containsStraight(const int hand[]);
bool  containsFullHouse(const int hand[]);
bool  containsFourOfaKind(const int hand[]);

//Global Variables
const int ARRAY_SIZE = 5;

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

int main() {
    
    int count;
    int number = 0;
    int hand[ARRAY_SIZE];
    
    count = 1;
    
    cout << "Enter five numeric cards, no face cards. Use 2 - 9 " << "\n";
    
    while (count <= 5){
        
        cout << "Card " << count << " : ";
        cin >> number;
        
        // hand is array that takes 5 number entered by user at console
        
        hand[count] = number;
        count++;
        
    }// end while

         /* I have written 6 different functions which searches same array when user enter numbers between 2-9.
            This logic below is written to filter results and present highest score poker hand. for e.g some results 
            has a pair, three of a kind and full house. So highest score if full house. */
    
         // Entry point of various functions
    
        if ((containsTwoPair(hand)== true) && (containsFourOfaKind(hand)==true) && (containsThreeOfaKind(hand)==true) && (containsFullHouse(hand)==true)) {
        
            cout << "contains Full House" << endl;
            
        }
    
        else if ((containsTwoPair(hand)== true) && (containsThreeOfaKind(hand)==true) && (containsFullHouse(hand)==true)) {
            
            cout << "contains Full House" << endl;
        
        }
    
        else if (containsFullHouse(hand)){
        
            cout << "contains Full House" << endl;
        
        }
    
        else if ((containsTwoPair(hand)) && (containsThreeOfaKind(hand)) && (containsFourOfaKind(hand))){
        
            cout << "contains Four of a Kind" << endl;
        
        }
    
        else if ((containsTwoPair(hand)== true) && (containsFourOfaKind(hand)==true)) {
        
            cout << "contains Four of a Kind" << endl;
        
        }
    
        else if (containsFourOfaKind(hand)) {
        
            cout << "contains Four of a Kind" << endl;
        
        }
    
        else if ((containsPair(hand)== true) && (containsThreeOfaKind(hand)==true)) {
        
            cout << "contains Three of a Kind" << endl;
        
        }
    
        else if (containsThreeOfaKind(hand)){
        
            cout << "contains Three of a Kind" << endl;
        
        }
    
        else if (containsTwoPair(hand) && containsStraight(hand)) {
    
                cout << "contains two pair" << endl;
                
        }
    
        else if (containsTwoPair(hand)) {
            
            cout << "contains two pair" << endl;
            
        }
    
        else if (containsPair(hand) && containsStraight(hand)) {
        
            cout << "contains a pair" << endl;
        
        }
    
        else if (containsPair(hand)){
        
            cout << "contains a pair" << endl;
        }
    
        else if (containsStraight(hand)){
        
            cout << "contains straight" << endl;
            
        } else {
    
            cout << "contains High" << endl;
        }// end if else

}//end of main

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
// Function Definition

/* Function containsPair check for pair value. The value in pair can be anywhere in hand. For e.g. 2,4,3,4,5. Here we
   see pair of 4 . Function then returns true result. If pair not found, function then returns false result. */

bool  containsPair(const int hand[]){
    
    bool result = false;
   
        
    if((hand[1]== hand[2]) && (hand[2] != hand[3]) && (hand [3] != hand[4]) && (hand[4] != hand[5])){
        
        result = true;
    }
    else if ((hand[2]== hand[3]) && (hand[3] != hand[4]) && (hand [4] != hand[5]) && (hand[5] != hand[1])){
        
        result = true;
    }
    else if((hand[3]== hand[4]) && (hand[4] != hand[5]) && (hand [5] != hand[1]) && (hand[1] != hand[2]) ){
        
        result = true;
    }
    else if((hand[1]== hand[3]) && (hand[3] != hand[2]) && (hand [2] != hand[4]) && (hand[4] != hand[5]) ){
       
        result = true;
    }
    else if((hand[2]== hand[4]) && (hand[4] != hand[3]) && (hand [3] != hand[5]) && (hand[5] != hand[1]) ){
        
        result = true;
    }
    else if((hand[3]== hand[5]) && (hand[5] != hand[4]) && (hand [4] != hand[1]) && (hand[1] != hand[2]) ){
       
        result = true;
    }
    else if((hand[1]== hand[4]) && (hand[4] != hand[2]) && (hand [2] != hand[3]) && (hand[3] != hand[5]) ){
        
        result = true;
    }
    else if((hand[2]== hand[5]) && (hand[5] != hand[1]) && (hand [1] != hand[3]) && (hand[3] != hand[4]) ){
        
        result = true;
    }
    else if((hand[4]== hand[5]) && (hand[5] != hand[3]) && (hand [3] != hand[2]) && (hand[2] != hand[1]) ){
        
        result = true;
    }
    else if((hand[1]== hand[5]) && (hand[5] != hand[2]) && (hand [2] != hand[3]) && (hand[3] != hand[4]) ){
        
        result = true;
    }
    else if((hand[5]== hand[1]) && (hand[1] != hand[2]) && (hand [2] != hand[4]) && (hand[4] != hand[3])) {
        
        result = true;
    }
    else{
            
                result = false;
    }// end if else
    
    return result;
} // end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/* Function containsTwoPair check for two pair value. The value in pair can be anywhere in hand. For e.g. 2,4,3,4,2. Here we
 see two pair . Function then returns true result. If pair not found, function then returns false result. */

bool containsTwoPair(const int hand[]){
    
    bool result = false;
    
    if( ((hand[1] == hand[2]) && (hand[3] == hand[4])) ||
        ((hand[1] == hand[2]) && (hand[3] == hand[5])) ||
        ((hand[1] == hand[2]) && (hand[4] == hand[5])) ||
       
        ((hand[1] == hand[3]) && (hand[2] == hand[4])) ||
        ((hand[1] == hand[3]) && (hand[2] == hand[5])) ||
        ((hand[1] == hand[3]) && (hand[4] == hand[5])) ||
       
        ((hand[1] == hand[4]) && (hand[2] == hand[3])) ||
        ((hand[1] == hand[4]) && (hand[2] == hand[5])) ||
        ((hand[1] == hand[4]) && (hand[3] == hand[5])) ||
       
        ((hand[1] == hand[5]) && (hand[2] == hand[3])) ||
        ((hand[1] == hand[5]) && (hand[2] == hand[4])) ||
        ((hand[1] == hand[5]) && (hand[3] == hand[4])) )
     {
        
        result = true;
         
     } else {
         result = false;
    
    } // end if else
    return result;
} // end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/* Function containsThreeOfaKind check for three similar value. The value can be anywhere in hand. For e.g. 2,2,3,4,2. Here we
 see three 2 pair . Function then returns true result. If not found, function then returns false result. */

bool  containsThreeOfaKind(const int hand[]){

    bool result = false;
    
    if((hand[3] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[1] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[1] == hand[2]) && (hand [2] == hand[5])){
        
        result = true;
    }
    else if((hand[1] == hand[2]) && (hand [2] == hand[3])){
        
        result = true;
    }
    else if((hand[2] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[1] == hand[3]) && (hand [3] == hand[5])){
        
        result = true;
    }
    else if((hand[2] == hand[3]) && (hand [3] == hand[5])){
        
        result = true;
    }
    else if((hand[2] == hand[3]) && (hand [3] == hand[4])){
        
        result = true;
    }
    else if((hand[1] == hand[3]) && (hand [3] == hand[4])){
        
        result = true;
    }
    else if((hand[1] == hand[2]) && (hand [2] == hand[4])){
        
        result = true;
    }
    else{
        result = false;
    } // end if else
    
    return result;
    
} // end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/* Function containsStraight check for consecutive values. The value in hand will be 2,3,4,5,6 or 5, 6, 2, 3,4. Here we
 consecutive values . Function then returns true result. If not found, function then returns false result. */

bool containsStraight(const int hand[]){
    
    bool result = false;
    
    for(int i = 1; i < 5; i++){
        
        if((hand[i]+1) == (hand[i+1])) {
           
            result = true;
            
        } else {
           
            result = false;
        } // end if else
    } // end for i

    return result;
} // end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/* Function containsFullHouse check for a pair value and three similar value. The value pair can be anywhere in hand. 
 For e.g. 2,2,3,3,3. or 3, 3, 2, 2, 3 or 2, 3, 3, 3, 2.
 Function then returns true result. If not found, function then returns false result. */

bool  containsFullHouse(const int hand[]){

    bool result = false;
    
    if((hand[1]== hand[2]) && (hand[3] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[2]== hand[3]) && (hand[1] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[3]== hand[4]) && (hand[1] == hand[2]) && (hand [2] == hand[5])){
        
        result = true;
    }
    else if((hand[4]== hand[5]) && (hand[1] == hand[2]) && (hand [2] == hand[3])){
        
        result = true;
    }
    else if((hand[1]== hand[3]) && (hand[2] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[2]== hand[4]) && (hand[1] == hand[3]) && (hand [3] == hand[5])){
        
        result = true;
    }
    else if((hand[1]== hand[4]) && (hand[2] == hand[3]) && (hand [3] == hand[5])){
        
        result = true;
    }
    else if((hand[1]== hand[5]) && (hand[2] == hand[3]) && (hand [3] == hand[4])){
        
        result = true;
    }
    else if((hand[2]== hand[5]) && (hand[1] == hand[3]) && (hand [3] == hand[4])){
        
        result = true;
    }
    else if((hand[3]== hand[5]) && (hand[1] == hand[2]) && (hand [2] == hand[4])){
        
        result = true;
    }
    else{
        result = false;
    } // end if else
    
    return result;
}// end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/* Function containsFourOfaKind check for four similar value. The value incan be anywhere in hand. For e.g. 4,4,3,4,4. Here we
 see four 4 . Function then returns true result. If not found, function then returns false result. */

bool  containsFourOfaKind(const int hand[]){
    
    bool result = false;
    
    if((hand[1]== hand[2]) && (hand[2] == hand[3]) && (hand [3] == hand[4])){
        
        result = true;
    }
    else if((hand[2]== hand[3]) && (hand[3] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[1]== hand[2]) && (hand[2] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[1]== hand[3]) && (hand[3] == hand[4]) && (hand [4] == hand[5])){
        
        result = true;
    }
    else if((hand[1]== hand[2]) && (hand[2] == hand[3]) && (hand [3] == hand[5])){
        
        result = true;
    }
    else{
        result = false;
    } // end if else
    
    return result;
}// end function

/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*                                                  Sample run of program                                                   */


/*1) Full House --> Enter : 3 , 3, 3, 2, 2 It Prints-->

Enter five numeric cards, no face cards. Use 2 - 9
Card 1 : 3
Card 2 : 3
Card 3 : 3
Card 4 : 2
Card 5 : 2
contains Full House
 
*/

/*2) Four of a Kind --> Enter : 3 , 3, 3, 3, 2 It Prints -->
 
 Enter five numeric cards, no face cards. Use 2 - 9
 Card 1 : 3
 Card 2 : 3
 Card 3 : 3
 Card 4 : 3
 Card 5 : 2
 contains Four of a Kind
 
*/

/*3) Three of a Kind -->Enter : 3, 3, 3, 2, 4. It Prints -->

 Enter five numeric cards, no face cards. Use 2 - 9
 Card 1 : 3
 Card 2 : 3
 Card 3 : 3
 Card 4 : 2
 Card 5 : 4
 contains Three of a Kind

*/

/*4) Two Pair --> Enter : 2, 2, 4, 4, 6. It Prints -->

 Enter five numeric cards, no face cards. Use 2 - 9
 Card 1 : 2
 Card 2 : 2
 Card 3 : 4
 Card 4 : 4
 Card 5 : 6
 contains two pair

*/

/*5) A Pair --> Enter : 2, 2, 4, 7, 8. It Prints -->
 
 Enter five numeric cards, no face cards. Use 2 - 9
 Card 1 : 2
 Card 2 : 2
 Card 3 : 4
 Card 4 : 7
 Card 5 : 8
 contains a pair
 
 */

/*6) Straight --> Enter : 2, 3, 4, 5, 6. It Prints -->
 
 Enter five numeric cards, no face cards. Use 2 - 9
 Card 1 : 2
 Card 2 : 3
 Card 3 : 4
 Card 4 : 5
 Card 5 : 6
 contains straight
 
 */

/*7) High --> Enter : 2, 3, 5, 7, 9. It Prints -->
 
 Enter five numeric cards, no face cards. Use 2 - 9
 Card 1 : 2
 Card 2 : 3
 Card 3 : 5
 Card 4 : 7
 Card 5 : 9
 contains High
 
 */


