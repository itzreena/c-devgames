/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Program filename : fraction.cpp
 Created by Reena Singh on 11/02/15.
 ReenaSing Copyright © 2016, All Right Reserved
 
 Program Description: A fraction class whose objects will represent fractions. On fraction objects we are using overloaded operator which is defined in this class. Overloaded operator will calculate product, division, addition, subtraction and check equality of two fraction. For these functions one of the fraction is the calling object and other fraction is the parameter to the function. The program is designed in such a way that fraction is always stored as an fraction. i.e num/den, weather it is passed to function or the result is returned from function. The private data member are numerator and denomenator. i.e 2/3 or num/den. 
 
 The overloaded operators will perform the following tests:-
 
 -relational test
 -basic test
 -binary arithmetic test
 -arithmetic assignment test
 
 -overloaded binary arithmetic operators are +, -, *, and / fractions.
 -overloaded relational operators that compares using are <, >, ==, <=, >=, !=.
 -overloaded arithmetic assignment operator that does maths are +=, *=, /=, -=.
 -overloaded increment and decrement operator ++, -- that does postfix and prefix on fraction.
 
 The three helper function: post and pre conditions.
 [1]int numerator() const;
 [2]int denominator() const;
 [3]int compare(const fraction &fracRight) const;
 
 one output function.
 [1]friend ostream& operator<<(ostream& out, const fraction &value);
 
 
 The eighteen member function is written for this program: post and pre conditions.
 
 [1]friend bool operator<(const fraction &fracLeft, const fraction &fracRight);
 [2]friend bool operator<=(const fraction &fracLeft, const fraction &fracRight);
 [3]friend bool operator==(const fraction &fracLeft, const fraction &fracRight);
 [4]friend bool operator!=(const fraction &fracLeft, const fraction &fracRight);
 [5]friend bool operator>=(const fraction &fracLeft, const fraction &fracRight);
 [6]friend bool operator>(const fraction &fracLeft, const fraction &fracRight);
 [7]friend fraction operator+(const fraction &fracLeft, const fraction &fracRight);
 [8]friend fraction operator-(const fraction &fracLeft, const fraction &fracRight);
 [9]friend fraction operator*(const fraction &fracLeft, const fraction &fracRight);
 [10]friend fraction operator/(const fraction &fracLeft, const fraction &fracRight);
 [11][fraction operator+=(const fraction &number);
 [12]fraction operator-=(const fraction &number);
 [13]fraction operator*=(const fraction &number);
 [14]fraction operator/=(const fraction &number);
 [15]fraction& operator++();
 [16]fraction operator++(int extra);
 [17]fraction& operator--();
 [18]fraction operator--(int extra);
 
 +++++++++++++Results on Run: +++++++++++++++++++++++
 
 ----- Testing basic fraction creation & printing
 fraction [0] = 4/8
 fraction [1] = -15/21
 fraction [2] = 10/1
 fraction [3] = 12/-3
 fraction [4] = 0/1
 fraction [5] = 28/6
 fraction [6] = 0/12
 
 ----- Testing relational operators between fractions
 Comparing 3/6 to 1/2
	Is left < right? false
	Is left <= right? true
	Is left > right? false
	Is left >= right? true
	Does left == right? true
	Does left != right ? false
 Comparing 1/2 to -15/30
	Is left < right? false
	Is left <= right? false
	Is left > right? true
	Is left >= right? true
	Does left == right? false
	Does left != right ? true
 Comparing -15/30 to 1/10
	Is left < right? true
	Is left <= right? true
	Is left > right? false
	Is left >= right? false
	Does left == right? false
	Does left != right ? true
 Comparing 1/10 to 0/1
	Is left < right? false
	Is left <= right? false
	Is left > right? true
	Is left >= right? true
	Does left == right? false
	Does left != right ? true
 Comparing 0/1 to 0/2
	Is left < right? false
	Is left <= right? true
	Is left > right? false
	Is left >= right? true
	Does left == right? true
	Does left != right ? false
 
 ----- Testing relations between fractions and integers
 Comparing -3/6 to 2
	Is left < right? true
	Is left <= right? true
	Is left > right? false
	Is left >= right? false
	Does left == right? false
	Does left != right ? true
 Comparing -3 to 1/4
	Is left < right? true
	Is left <= right? true
	Is left > right? false
	Is left >= right? false
	Does left == right? false
	Does left != right ? true
 
 ----- Testing binary arithmetic between fractions
 1/6 + 1/3 = 9/18
 1/6 - 1/3 = -3/18
 1/6 * 1/3 = 1/18
 1/6 / 1/3 = 3/6
 1/3 + -2/3 = -3/9
 1/3 - -2/3 = 9/9
 1/3 * -2/3 = -2/9
 1/3 / -2/3 = 3/-6
 -2/3 + 5/1 = 13/3
 -2/3 - 5/1 = -17/3
 -2/3 * 5/1 = -10/3
 -2/3 / 5/1 = -2/15
 5/1 + -4/3 = 11/3
 5/1 - -4/3 = 19/3
 5/1 * -4/3 = -20/3
 5/1 / -4/3 = 15/-4
 
 ----- Testing arithmetic between fractions and integers
 -1/2 + 4 = 7/2
 -1/2 - 4 = -9/2
 -1/2 * 4 = -4/2
 -1/2 / 4 = -1/8
 3 + -1/2 = 5/2
 3 - -1/2 = 7/2
 3 * -1/2 = -3/2
 3 / -1/2 = 6/-1
 
 ----- Testing shorthand arithmetic assignment on fractions
 1/6 += 4/1 = 25/6
 25/6 -= 4/1 = 1/6
 1/6 *= 4/1 = 4/6
 4/6 /= 4/1 = 4/24
 4/1 += -1/2 = 7/2
 7/2 -= -1/2 = 16/4
 16/4 *= -1/2 = -16/8
 -16/8 /= -1/2 = -32/-8
 -1/2 += 5/1 = 9/2
 9/2 -= 5/1 = -1/2
 -1/2 *= 5/1 = -5/2
 -5/2 /= 5/1 = -5/10
 
 ----- Testing shorthand arithmetic assignment using integers
 -1/3 += 3 = 8/3
 8/3 -= 3 = -1/3
 -1/3 *= 3 = -3/3
 -3/3 /= 3 = -3/9
 
 ----- Testing increment/decrement prefix and postfix
 Now g = -1/3
 g++ = -1/3
 Now g = 2/3
 ++g = 5/3
 Now g = 5/3
 g-- = 5/3
 Now g = 2/3
 --g = -1/3
 Now g = -1/3

 
 */
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/

#include<iostream>
#include "fraction.h"

using namespace std;

//Default construtor takes no parameter : initialize num to 0 and den to 1.
fraction::fraction(){
    num = 0;
    den = 1;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//The parameterized construtor takes two parameter : inNum(numerator) and inDen(denomenator) and then it store in num and den

fraction::fraction(int inNum, int inDen)
{
    num = inNum;
    den = inDen;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//The parameterized construtor takes one parameter : number and then it store in num, den is initialize to 1.

fraction::fraction(int number)
{
    num = number;
    den = 1;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//returns num, numerator value

int fraction::numerator() const
{
    return num;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//returns den, denominator value

int fraction::denominator() const
{
    return den;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator + function, takes address of two fraction which is kept const. This is done so that when actual value
//is passed in the function it does not changes. It wants to perform addition of two fraction and then return value in fraction.

fraction operator+(const fraction &fracLeft, const fraction &fracRight)
{
    fraction result(fracLeft.numerator() * fracRight.denominator()
                    + fracRight.numerator() * fracLeft.denominator(),
                    fracLeft.denominator() * fracRight.denominator());
    return result;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator - function, takes address of two fraction which is kept const. This is done so that actual value when
//passed in the function it does not changes. It wants to perform subtraction of two fraction and then return value in fraction.

fraction operator-(const fraction &fracLeft, const fraction &fracRight)
{
    fraction result(fracLeft.numerator() * fracRight.denominator()
                    - fracRight.numerator() * fracLeft.denominator(),
                    fracLeft.denominator() * fracRight.denominator());
    return result;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator * function, takes address of two fraction which is kept const. This is done so that actual value when
//passed in the function it does not changes. It wants to perform multiplication of two fraction and then return value in fraction.

fraction operator*(const fraction &fracLeft, const fraction &fracRight)
{
    fraction result(fracLeft.numerator() * fracRight.numerator(),
                    fracLeft.denominator() * fracRight.denominator());
    return result;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator / function, takes address of two fraction which is kept const. This is done so that actual value when
//passed in the function it does not changes. It wants to perform division of two fraction and then return value in fraction.

fraction operator/(const fraction &fracLeft, const fraction &fracRight)
{
    fraction result(fracLeft.numerator() * fracRight.denominator(),
                    fracLeft.denominator() * fracRight.numerator());
    return result;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//function compare takes address of fraction and subtract it with the calling fraction object and return values in int format.
//Can be zero, +ve or -ve

int fraction::compare(const fraction &fracRight) const
{
    return numerator() * fracRight.denominator() -
    denominator() * fracRight.numerator();
    
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator < function contains addresses of two valid fractions:- fracLeft and fracRight.
//function uses helper function compare. It return's true, if fracLeft < fracRight,  else it return's false.


bool operator<(const fraction &fracLeft, const fraction &fracRight)
{
    return fracLeft.compare(fracRight) < 0;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator <= function contains addresses of two valid fractions:- fracLeft and fracRight.
//function uses helper function compare. It return's true, if fracLeft <= fracRight,  else it return's false.

bool operator<=(const fraction &fracLeft, const fraction &fracRight)
{
    return fracLeft.compare(fracRight) <= 0;
}




/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator == function contains addresses of two valid fractions:- fracLeft and fracRight.
//function uses helper function compare. It return's true, if fracLeft == fracRight,  else it return's false.

bool operator==(const fraction &fracLeft, const fraction &fracRight)
{
    return fracLeft.compare(fracRight) == 0;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator != function contains addresses of two valid fractions:- fracLeft and fracRight.
//function uses helper function compare. It return's true, if fracLeft != fracRight,  else it return's false.

bool operator!=(const fraction &fracLeft, const fraction &fracRight)
{
    return fracLeft.compare(fracRight) != 0;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator >= function contains addresses of two valid fractions:- fracLeft and fracRight.
//function uses helper function compare. It return's true, if fracLeft >= fracRight,  else it return's false.

bool operator>=(const fraction &fracLeft, const fraction &fracRight)
{
    return fracLeft.compare(fracRight) >= 0;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator > function contains addresses of two valid fractions:- fracLeft and fracRight.
//function uses helper function compare. It return's true, if fracLeft > fracRight,  else it return's false.

bool operator>(const fraction &fracLeft, const fraction &fracRight)
{
    return fracLeft.compare(fracRight) > 0;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator << function, works like cout. function takes address of computed fraction value and produce output..
// in fraction form num/den, using out..an ostream object.

ostream& operator<<(ostream& out, const fraction &value)
{
    out << value.numerator() << "/" << value.denominator();
    return out;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//increment fraction by 1, returns unchange value

fraction& fraction::operator++()
{
    num += den;
    return *this;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//increment fraction by 1, returns unchange value

fraction fraction::operator++(int unchanged)
{
    
    fraction unchange(num, den);
    num += den;
    return unchange;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//decrement fraction by 1, returns new value

fraction& fraction::operator--()
{
    num -= den;
    return *this;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//decrement fraction by 1, returns unchange value


fraction fraction::operator--(int unchanged)
{
    fraction unchange(num, den);
    num -= den;
    return unchange;
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator -= function subtract the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.

fraction fraction::operator-=(const fraction &number){
    
    num = (num * number.den)-(number.num*den);
    den = den * number.den;
    return *this;
    
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator += function add the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.

fraction fraction::operator+=(const fraction &number){
  
    num = (num * number.den) + (number.num * den);
    den = den * number.den;
    return *this;
   
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator /= function divides the number( parameter object's called by reference) to the calling object. This is done by cross multiplication with denominator to calling object numerator and assigning to calling object numerator. Again numerator is multiplied to calling object denominator and then returned to calling object denominator. Lastly returning *this meaning complete calling object in form of num/den.

fraction fraction::operator/=(const fraction &number){
    
    num = num * number.den;
    den = den * number.num;
    return *this;
    
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
//Overloaded operator *= function multiply the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.

fraction fraction::operator*=(const fraction &number){

    
    num = num * number.num;
    den = den * number.den;
    return *this;
   
}





/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/


