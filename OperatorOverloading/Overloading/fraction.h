/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/
/*
 Specification filename : fraction.h
 Created by Reena Singh on 11/01/15. 
 ReenaSing Copyright © 2016, All Right Reserved
 
 Description:  This file gives the specification of the fraction abstract data type.
 
 There are three constructors: one takes no parameter and others takes one parameter and two parameters.
 
 A fraction class whose objects will represent fractions. On the fraction objects we are using overloaded operators that will perform relational test, binary arithmetic test, arithmetic assignment test.

 -overloaded binary arithmetic operations that +, -, *, and / fractions.
 -overloaded relational operators that compares using <, >, ==, <=, >=, !=.
 -overloaded arithmetic assignment operator that does maths +=, *=, /=, -=.
 -overloaded increment and decrement operator ++, -- that does postfix and prefix on fration.
 
 -An output ostream operation << displays the value of a fraction object on the screen in the form numerator/denominator (i.e fraction).
 
 -two data members(private), num to represent the numerator of the fraction being represented, and den to represent the denominator of the fraction being represented.
 
 
 The three constructors: post and pre conditions.
 
 [1]fraction();
 Postcondition: By default num = 0 and den = 0.
 
 [2]fraction(int number);
 Postcondition: It takes one parameter number(numerator), then it store in num. den is predefined with value = 1.
 
 [3]fraction(int inNum = 0, int inDen = 1);
 Postcondition: It takes two parameter inNum(numerator) and inDen(denomenator) and then it store in num and den.
 
 The three helper function: post and pre conditions.
 [1]int numerator() const;
 Postcondition: helps to get the value of private num (numerator) when called by member functions.
 
 [2]int denominator() const;
 Postcondition: helps to get the value of private den (numerator) when called by member functions.
 
 [3]int compare(const fraction &fracRight) const;
 Postcondition: compare's right fraction value to left fraction value when testing with relational overloaded operator.
 
 one output function.
 [1]friend ostream& operator<<(ostream& out, const fraction &value);
 Precondition: The out of ostrem object is waiting to receive the fraction output.
 Postcondition: fraction has been output into the ostream out in the form of num/den.
 
 The eighteen member function is written for this program: post and pre conditions.
 const is used so that parameter is not changed when pass by ref. from [1] to [14]
 
 [1]friend bool operator<(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition: function return's true, if fracLeft < fracRight,  else it return's false.
 
 [2]friend bool operator<=(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition: function return's true, if fracLeft <= fracRight,  else it return's false.
 
 [3]friend bool operator==(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition: function return's true, if fracLeft == fracRight,  else it return's false.
 
 [4]friend bool operator!=(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition: function return's true, if fracLeft != fracRight,  else it return's false.
 
 [5]friend bool operator>=(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition: function return's true, if fracLeft >= fracRight,  else it return's false.
 
 [6]friend bool operator>(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition:- function return's true, if fracLeft > fracRight,  else it return's false.
 
 [7]friend fraction operator+(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses two valid fractions:- fracLeft and fracRight.
 Postcondition:- Returns addition of two fraction
 
 [8]friend fraction operator-(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition:- Returns subtraction of two fraction
 
 [9]friend fraction operator*(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition:- Returns Product of two fraction
 
 
 [10]friend fraction operator/(const fraction &fracLeft, const fraction &fracRight);
 Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
 Postcondition:- Returns division of two fraction
 
 [11][fraction operator+=(const fraction &number);
 Precondition:- Input parameter contains an address to fraction number which is const.
 Postcondition:- First it adds the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
 
 [12]fraction operator-=(const fraction &number);
 Precondition:- Input parameter contains an address to fraction number which is const.
 Postcondition:- First it subtract the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
 
 [13]fraction operator*=(const fraction &number);
 Precondition:- Input parameter contains an address to fraction number which is const.
 Postcondition:- First it multiplication the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
 
 [14]fraction operator/=(const fraction &number);
 Precondition:- Input parameter contains an address to fraction number
 Postcondition:- First it divide the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
 
 [15]fraction& operator++();
 Precondition:- Input parameter contains a valid fraction number
 Postcondition:- Increment by one and return new value of fraction.
 
 [16]fraction operator++(int extra);
 Precondition:- Input parameter contains a valid fraction number
 Postcondition:- Increment by one and return new value of fraction.
 
 [17]fraction& operator--();
 Precondition:- Input parameter contains a valid fraction number
 Postcondition:- Decrement by one and return new value of fraction.
 
 [18]fraction operator--(int extra);
 Precondition:- Input parameter contains a valid fraction number
 Postcondition:- Decrement by one but return same value of fraction.
 
*/
/*~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#**~#*~#*~#*~#*~#*~#*~#*~#*~#*~#*/




#ifndef fraction_H
#define fraction_H

#include <iostream>

using namespace std;

class fraction {
public:
    
    /*******(Default constructor)********/
    
    fraction();
    // Default Constructor:-
    // Postcondition:
    // num = 0 && den = 1
    
    
    
    /*******(Parameterized constructor)********/
    
    fraction(int number);
    // Constructor:-
    // Postcondition:
    //num is stored according to incoming parameter number, den = 1
    
    
    
    
    fraction(int inNum, int inDen);
    // Constructor:-
    // Postcondition:
    //num and den is stored according to incoming parameter inNum and inDen
    
    
    
    
    /**********(Helper functions)*************/
    
    
    int numerator() const;
    //Postcondition: helps to get the value of private num (numerator) when called by member functions.
    
    
    
    
    int denominator() const;
    //Postcondition: helps to get the value of private den (numerator) when called by member functions.

    
    
    
    
    int compare(const fraction &fracRight) const;
    //Postcondition: subtract right fraction value with calling object fraction value and produce results in int format..
    // the values are +ve, -ve or zero. This is helper function which helps to check the condition is true or false with..
    // overloaded relational test.

    
    
    
    
    /***********(Member Function)****************/
    
    
    friend bool operator<(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition: function return's true, if fracLeft < fracRight,  else it return's false.
    
    
    
    friend bool operator<=(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition: function return's true, if fracLeft <= fracRight,  else it return's false.
    
    
    
    
    friend bool operator==(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition: function return's true, if fracLeft == fracRight,  else it return's false.
    
    
    
    
    friend bool operator!=(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition: function return's true, if fracLeft != fracRight,  else it return's false.
    
    
    
    
    friend bool operator>=(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition: function return's true, if fracLeft >= fracRight,  else it return's false.
    
    
    
    
    friend bool operator>(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition:- function return's true, if fracLeft > fracRight,  else it return's false.
    
    
    
    
    friend fraction operator+(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses two valid fractions:- fracLeft and fracRight.
    //Postcondition:- Returns addition of two fraction
    
    
    
    
    friend fraction operator-(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition:- Returns subtraction of two fraction
    
    
    
    
    friend fraction operator*(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition:- Returns Product of two fraction
    
    
    
    
    friend fraction operator/(const fraction &fracLeft, const fraction &fracRight);
    //Precondition:- Input parameters contains addresses of two valid fractions:- fracLeft and fracRight.
    //Postcondition:- Returns division of two fraction
    
    
    
    
    fraction operator+=(const fraction &number);
    //Precondition:- Input parameter contains an address to fraction number which is const.
    //Postcondition:- First it adds the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
    
    
    
    
    
    fraction operator-=(const fraction &number);
    //Precondition:- Input parameter contains an address to fraction number which is const.
    //Postcondition:- First it subtract the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
    
    
    
    
    
    fraction operator*=(const fraction &number);
    //Precondition:- Input parameter contains an address to fraction number which is const.
    //Postcondition:- First it multiplication the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
    
    
    
    
    fraction operator/=(const fraction &number);
    //Precondition:- Input parameter contains an address to fraction number
    //Postcondition:- First it divide the number( parameter object's called by reference) to the calling object. Second, it returns the value that it just assigned to the calling object.
    
    
    
    
    
    fraction& operator++();
    //Precondition:- Input parameter contains a valid fraction number
    //Postcondition:- Increment by one and return new value of fraction.
    
    
    
    
    
    fraction operator++(int unchanged);
    //Precondition:- Input parameter contains a valid fraction number
    //Postcondition:- Increment by one and return new value of fraction.
    
    
    
    
    
    fraction& operator--();
    //Precondition:- Input parameter contains a valid fraction number
    //Postcondition:- Decrement by one and return new value of fraction.

    
    
    
    fraction operator--(int unchanged);
    //Precondition:- Input parameter contains a valid fraction number
    //Postcondition:- Decrement by one but return same value of fraction.
    
    
    
    
    
    /************(Displays output)*******/
    
    friend ostream& operator<<(ostream& out, const fraction &value);
    //Precondition: The out of ostrem object is waiting to receive the fraction output.
    //Postcondition: fraction been outputed into the ostream out in the form of num/den.
    
    
    
    
    
    //*******(Data member)*******/
private:
    
    int num; // private member numerator
    int den; // private member denomenator
};


#endif
